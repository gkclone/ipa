; GK Distro
includes[gk_distro] = http://code.gkdigital.co/gk_distro/v2.0.22/gk_distro.make

; MODULES ======================================================================

; Standard
projects[gk_age_restriction][type] = module
projects[gk_age_restriction][subdir] = standard
projects[gk_age_restriction][download][type] = git
projects[gk_age_restriction][download][url] = git@bitbucket.org:greeneking/gk-age-restriction.git
projects[gk_age_restriction][download][tag] = 7.x-1.0

projects[gk_articles][type] = module
projects[gk_articles][subdir] = standard
projects[gk_articles][download][type] = git
projects[gk_articles][download][url] = git@bitbucket.org:greeneking/gk-articles.git
projects[gk_articles][download][tag] = 7.x-2.0

projects[gk_members][type] = module
projects[gk_members][subdir] = standard
projects[gk_members][download][type] = git
projects[gk_members][download][url] = git@bitbucket.org:greeneking/gk-members.git
projects[gk_members][download][tag] = 7.x-2.1

projects[gk_promotions][type] = module
projects[gk_promotions][subdir] = standard
projects[gk_promotions][download][type] = git
projects[gk_promotions][download][url] = git@bitbucket.org:greeneking/gk-promotions.git
projects[gk_promotions][download][tag] = 7.x-2.5

projects[gk_rewards][type] = module
projects[gk_rewards][subdir] = standard
projects[gk_rewards][download][type] = git
projects[gk_rewards][download][url] = git@bitbucket.org:greeneking/gk-rewards.git
projects[gk_rewards][download][tag] = 7.x-2.3

projects[gk_social][type] = module
projects[gk_social][subdir] = standard
projects[gk_social][download][type] = git
projects[gk_social][download][url] = git@bitbucket.org:greeneking/gk-social.git
projects[gk_social][download][tag] = 7.x-3.0

projects[gk_tiers][type] = module
projects[gk_tiers][subdir] = standard
projects[gk_tiers][download][type] = git
projects[gk_tiers][download][url] = git@bitbucket.org:greeneking/gk-tiers.git
projects[gk_tiers][download][tag] = 7.x-1.3

projects[gk_webform][type] = module
projects[gk_webform][subdir] = standard
projects[gk_webform][download][type] = git
projects[gk_webform][download][url] = git@bitbucket.org:greeneking/gk-webform.git
projects[gk_webform][download][tag] = 7.x-2.3

; Contrib
projects[sharethis][type] = module
projects[sharethis][subdir] = contrib
projects[sharethis][download][type] = get
projects[sharethis][download][url] = http://ftp.drupal.org/files/projects/sharethis-7.x-2.10.tar.gz
projects[sharethis][download][tag] = 7.x-2.10

; LIBRARIES  ===================================================================

projects[cycle2.carousel][type] = library
projects[cycle2.carousel][subdir] = ""
projects[cycle2.carousel][download][type] = file
projects[cycle2.carousel][download][url] = http://malsup.github.io/min/jquery.cycle2.carousel.min.js

projects[picturefill][type] = library
projects[picturefill][subdir] = ""
projects[picturefill][download][type] = git
projects[picturefill][download][url] = https://github.com/scottjehl/picturefill.git

projects[style_guide_ipa][type] = library
projects[style_guide_ipa][subdir] = ""
projects[style_guide_ipa][download][type] = git
projects[style_guide_ipa][download][url] = git@bitbucket.org:greeneking/ipa-style-guide.git
projects[style_guide_ipa][download][tag] = v1.0.2
