(function($) {
  'use strict';

  Drupal.behaviors.premium_locals = {
    attach: function(context, settings) {
      // Run JS from the styleguide.
      if (typeof styleguide === 'object') {
        styleguide.init($);
      };
    }
  }

})(jQuery);
