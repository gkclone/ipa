<?php

/**
 * Implements hook_form_alter().
 */
function ipa_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['#node']) && $form['#node']->type == 'webform' && !path_is_admin(current_path())) {
    $machine_name = gk_machine_name_lookup_by_entity($form['#node']->nid);

    // Arrange items on the contact form into a grid.
    if ($machine_name == 'ipa_webform_contact') {
      $form['submitted']['#theme_wrappers'] = array('minima_grid');
      $form['submitted']['#grid_attributes'] = array(
        'class' => array('Grid--space'),
      );

      $form['submitted']['title']['#prefix'] = '<div class="Grid-cell u-lg-size1of2">';
      $form['submitted']['last_name']['#suffix'] = '</div>';

      $form['submitted']['email']['#prefix'] = '<div class="Grid-cell u-lg-size1of2">';
      $form['submitted']['postcode']['#suffix'] = '</div>';

      // Put a grid cell around the remaining fields, starting with 'question'.
      $form['submitted']['question']['#prefix'] = '<div class="Grid-cell">';

      $children = element_children($form['submitted'], TRUE);
      $form['submitted'][end($children)]['#suffix'] = '</div>';
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function ipa_form_gk_rewards_user_details_form_alter(&$form, &$form_state, $form_id) {
  // Rearrange name fields into a grid...
  $form['name']['#theme_wrappers'] = array('minima_grid');
  $form['name']['#grid_attributes']['class'] = array('Grid--spaceHorizontal');
  unset($form['name']['#type']);

  // ...name fields.
  $form['name']['fields'] = array(
    'label' => $form['name']['label'],
    'title' => $form['name']['title'],
    'first_name' => $form['name']['first_name'],
    'last_name' => $form['name']['last_name'],
  );

  unset($form['name']['label']);
  unset($form['name']['title']);
  unset($form['name']['first_name']);
  unset($form['name']['last_name']);

  // ...name label.
  $form['name']['fields']['label']['#type'] = 'item';
  $form['name']['fields']['label']['#title'] = t('Name');
  $form['name']['fields']['label']['#theme_wrappers'] = array('form_element', 'minima_grid_cell');
  $form['name']['fields']['label']['#grid_cell_attributes']['class'] = array('u-xl-size1of8', 'u-lg-size1of8', 'u-xs-sizeFull', 'u-sm-sizeFull');
  unset($form['name']['fields']['label']['#prefix']);

  // ...title.
  $form['name']['fields']['title']['#theme_wrappers'] = array('form_element', 'minima_grid_cell');
  $form['name']['fields']['title']['#grid_cell_attributes']['class'] = array('u-xl-size4of16', 'u-lg-size4of16', 'u-xs-sizeFull', 'u-sm-sizeFull');
  $form['name']['fields']['title']['#attributes']['required'] = TRUE;

  // ...first name.
  $form['name']['fields']['first_name']['#theme_wrappers'] = array('form_element', 'minima_grid_cell');
  $form['name']['fields']['first_name']['#grid_cell_attributes']['class'] = array('u-xl-size5of16', 'u-lg-size5of16', 'u-xs-sizeFull', 'u-sm-sizeFull');
  $form['name']['fields']['first_name']['#attributes']['required'] = TRUE;

  // ...last name.
  $form['name']['fields']['last_name']['#theme_wrappers'] = array('form_element', 'minima_grid_cell');
  $form['name']['fields']['last_name']['#grid_cell_attributes']['class'] = array('u-xl-size5of16', 'u-lg-size5of16', 'u-xs-sizeFull', 'u-sm-sizeFull');
  $form['name']['fields']['last_name']['#attributes']['required'] = TRUE;

  // Set grid cell classes and attributes for other fields...
  $size_label = array('u-xl-size1of8', 'u-lg-size1of8', 'u-md-size1of5', 'u-lg-size1of6', 'u-xs-sizeFull', 'u-sm-sizeFull');
  $size_field = array('u-xl-size7of8', 'u-lg-size7of8', 'u-md-size4of5', 'u-lg-size5of6', 'u-xs-sizeFull', 'u-sm-sizeFull');

  // ...email.
  $form['mail']['#title_grid_cell_attributes']['class'] = $size_label;
  $form['mail']['#field_grid_cell_attributes']['class'] = $size_field;
  $form['mail']['#attributes']['placeholder'] = 'E.g. yourname@example.com';
  $form['mail']['#attributes']['required'] = TRUE;
  unset($form['confirm_mail']);

  // ...postcode.
  $form['postcode']['#title_grid_cell_attributes']['class'] = $size_label;
  $form['postcode']['#field_grid_cell_attributes']['class'] = $size_field;
  $form['postcode']['#attributes']['placeholder'] = 'E.g. IP33 1QT';
  $form['postcode']['#attributes']['required'] = TRUE;
  $form['postcode']['#attributes']['pattern'] = '[A-Za-z]{1,2}[0-9Rr][0-9A-Za-z]? ?[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}';

  // ...date of birth.
  $form['date_of_birth']['#title_grid_cell_attributes']['class'] = $size_label;
  $form['date_of_birth']['#field_grid_cell_attributes']['class'] = $size_field;

  // ...communications opt-in.
  $form['comms_opt_in_other']['#title_grid_cell_attributes']['class'] = array('u-xl-size9of10', 'u-lg-size9of10');
  $form['comms_opt_in_other']['#field_grid_cell_attributes']['class'] = array('u-xl-size1of10', 'u-lg-size1of10');
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function ipa_form_gk_members_signup_cta_form_alter(&$form, &$form_state, $form_id) {
  $form['#attributes']['class'][] = 'Form--membersSignUp';

  // Add a title element.
  $form['title'] = array(
    '#theme' => 'html_tag',
    '#tag' => 'div',
    '#value' => t('Recieve the latest in IPA news, competitions and offers'),
    '#weight' => -10,
    '#attributes' => array(
      'class' => array(
        'Form-title',
      ),
    ),
  );
}

/**
 * Implements hook_theme_registry_alter().
 */
function ipa_theme_registry_alter(&$theme_registry) {
  // Override the template for the pane_content_header theme.
  $theme_registry['pane_content_header']['path'] = drupal_get_path('theme', 'ipa') . '/templates';
}

/**
 * Preprocess variables for theme html.
 */
function ipa_preprocess_html(&$variables) {
  if (($node = menu_get_object()) && $body_class_field = field_get_items('node', $node, 'field_body_class')) {
    $variables['attributes_array']['class'][] = $body_class_field[0]['value'];
  }
}

/**
 * Process variables for the html template.
 */
function ipa_process_html(&$variables, $hook) {
  // Add CSS to the page.
  $less_settings = less_get_settings('ipa');
  $minima_path = libraries_get_path('minima');
  $less_settings['variables']['minima-path'] = '~"' . $minima_path . '/src/less"';
  $style_guide_path = libraries_get_path('style_guide_ipa');

  drupal_add_css($style_guide_path . '/src/less/app.less', array(
    'less' => $less_settings,
  ));

  drupal_add_css($style_guide_path . '/src/less/ie.less', array(
    'browsers' => array(
      'IE' => 'lt IE 9',
      '!IE' => FALSE,
    ),
    'less' => $less_settings,
  ));

  $variables['styles'] = drupal_get_css();

  // Add JS to the page.
  drupal_add_js($minima_path . '/dist/js/minima.offcanvas.min.js');
  drupal_add_js($style_guide_path . '/src/js/moderizr.custom.js');
  drupal_add_js($style_guide_path . '/src/js/styleguide.js');
  drupal_add_js($style_guide_path . '/src/js/quicklook.js');
  drupal_add_js($variables['directory'] . '/js/ipa.js');
  drupal_add_js(libraries_get_path('cycle2') . '/jquery.cycle2.min.js');
  drupal_add_js(libraries_get_path('cycle2.carousel') . '/jquery.cycle2.carousel.min.js');
  drupal_add_js(libraries_get_path('cycle2.swipe') . '/jquery.cycle2.swipe.min.js');
  drupal_add_js(libraries_get_path('picturefill') . '/dist/picturefill.min.js');

  $variables['scripts'] = drupal_get_js();
}

/**
 * Preprocess variables for entities
 */
function ipa_preprocess_entity(&$variables) {
  if ($variables['entity_type'] == 'promotion_group') {
    // Add grid classes to promotions.
    $format = reset($variables['field_promotion_group_format']);
    $format = $format['value'];

    if ($format == 'default') {
      $promotions = $variables['promotions'];
      $variables['promotions'] = array(
        'promotions' => $promotions,
      );

      // Work out how to space grid elements.
      $cell_count = count($variables['promotions']['promotions']);
      $cell_count = ($cell_count > 4) ? 4 : $cell_count;

      // Wrap each promotion in a grid cell.
      foreach ($variables['promotions']['promotions'] as $key => $promotion) {
        $variables['promotions']['promotions'][$key]['#theme_wrappers'] = array('minima_grid_cell');
        $variables['promotions']['promotions'][$key]['#grid_cell_attributes'] = array(
          'class' => array("u-lg-size1of$cell_count", "u-ie-size1of$cell_count"),
        );
      }

      // Wrap all promotions in a grid.
      $variables['promotions']['promotions']['#theme_wrappers'] = array('minima_grid');
    }
  }
  elseif ($variables['entity_type'] == 'promotion' && $variables['elements']['#bundle'] == 'text_image') {
    $promotion = $variables['elements']['#entity'];

    if ($image = field_get_items('promotion', $promotion, 'field_promotion_image')) {
      $variables['attributes_array']['style'][] = 'background-image: url(' . file_create_url($image[0]['uri']) . ');';
    }
  }
}

/**
 * Preprocess variables for theme node.
 */
function ipa_preprocess_node(&$variables) {
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__' . $variables['view_mode'];

  if ($variables['type'] == 'product') {
    // Removing extra markup from fields: abv/bitter/sweet
    $abv_field = field_get_items('node', $variables['node'], 'field_product_abv');
    $variables['content']['product_abv_value'] = $abv_field[0]['value'];

    $bitter_field = field_get_items('node', $variables['node'], 'field_product_bitter');
    $variables['content']['product_bitter_value'] = field_view_value('node', $variables['node'], 'field_product_bitter', $bitter_field[0]);

    $sweet_field = field_get_items('node', $variables['node'], 'field_product_sweet');
    $variables['content']['product_sweet_value'] = field_view_value('node', $variables['node'], 'field_product_sweet', $sweet_field[0]);

    switch ($variables['view_mode']) {
      case 'full':
        // Don't display the main page title on full view. We'll put this in the
        // node template in order to adhere to the design.
        drupal_set_title('');

        // Display a link to the Greene King Shop website.
        $shop_link_title = t('Buy !title', array(
          '!title' => $variables['title'],
        ));

        $shop_link_url = 'http://www.greenekingshop.co.uk';

        if ($shop_url_field = field_get_items('node', $variables['node'], 'field_product_shop_url')) {
          $shop_link_url = $shop_url_field[0]['value'];
        }

        $variables['shop_link'] = l($shop_link_title, $shop_link_url, array(
          'attributes' => array(
            'class' => array('Button'),
            'target' => '_blank',
          ),
        ));
      break;

      case 'teaser':
        $variables['attributes_array']['class'][] = 'Product';

        // Add link attributes
        $variables['link_attributes'] = drupal_attributes(array(
          'href' => $variables['node_url'],
          'title' => $variables['elements']['#node']->title,
        ));
      break;
    }
  }
  elseif ($variables['type'] == 'article') {
    // Format the date.
    $timestamp = $variables['content']['field_article_date']['#items'][0]['value'];
    $variables['date'] = format_date($timestamp, 'custom', 'jS F Y');

    if ($image_field = field_get_items('node', $variables['node'], 'field_article_image')) {
      $image = theme('image', array(
        'path' => $image_field[0]['uri'],
      ));

      $variables['image'] = l($image, 'node/' . $variables['nid'], array(
        'html' => TRUE,
      ));
    }

    // Add grid classes to all displays except the full view.
    if ($variables['view_mode'] == 'teaser' || $variables['view_mode'] == 'promote') {
      $variables['content_attributes_array']['class'][] = 'Grid';
      $variables['content_attributes_array']['class'][] = 'Grid--spaceHorizontal';
    }

    // Add a 'Read more' link to articles displayed with the promoted view mode.
    if ($variables['view_mode'] == 'promote') {
      $node_title_stripped = strip_tags($variables['node']->title);

      $variables['content']['links']['node']['#links']['node-readmore'] = array(
        'title' => t('Read more<span class="is-hiddenVisually"> about @title</span>', array(
          '@title' => $node_title_stripped,
        )),
        'href' => 'node/' . $variables['node']->nid,
        'html' => TRUE,
        'attributes' => array(
          'rel' => 'tag',
          'title' => $node_title_stripped,
        ),
      );
    }
  }

  // Alter the 'Read more' links on nodes.
  if (isset($variables['content']['links']['node']['#links']['node-readmore'])) {
    $link = &$variables['content']['links']['node']['#links']['node-readmore'];
    $link['attributes']['class'][] = 'Button';

    if ($variables['type'] == 'product') {
      $link['title'] = t('Discover !title', array(
        '!title' => $variables['node']->title,
      ));
    }
    elseif ($variables['type'] == 'article') {
      $link['attributes']['class'][] = 'Button--secondary';
    }
  }
}

/**
 * Preprocess variables for theme panels_pane.
 */
function ipa_preprocess_panels_pane(&$variables) {
  $pane = $variables['pane'];

  switch ($pane->panel) {
    case 'header':
      // Display only the content for the main menu pane.
      if ($pane->subtype == 'menu_block-gk-core-main-menu') {
        $variables['theme_hook_suggestion'] = 'box__offcanvas_primary';
      }
    break;

    case 'primary':
      // Render all panes in the primary region as containers (except when
      // looking at article nodes since we use a different layout there).
      if (($node = menu_get_object()) && $node->type != 'article') {
        $variables['theme_hook_suggestions'][] = 'box__contained';

        $variables['container_attributes_array']['class'][] = 'Container';
        $variables['container_attributes_array']['class'][] = 'Container--box';

        if ($pane->type == 'entity_view' && $pane->subtype == 'node') {
          $variables['container_attributes_array']['class'][] = 'Container--renderedNode';
        }
      }

      // Add useful container classes to other panes.
      if ($pane->type == 'gk_articles_categories') {
        $variables['container_attributes_array']['class'][] = 'Container--articleCategories';
      }
      elseif ($pane->type == 'gk_articles_list') {
        if ($pane->configuration['promote'] == 1) {
          $variables['container_attributes_array']['class'][] = 'Container--articlesListPromoted';
        }
        elseif ($pane->configuration['sticky'] == 1) {
          $variables['container_attributes_array']['class'][] = 'Container--articlesListSticky';
        }
      }
    break;

    // Add footer grid classes.
    case 'footer':
      $grid_cell_classes = '';

      switch ($pane->subtype) {
        case 'menu-footer-menu':
          $grid_cell_classes = 'u-sm-size1of2 u-lg-size1of4 u-ie-size1of4';
        break;

        case 'menu_block-ipa-core-footer-secondary-menu':
          $grid_cell_classes = 'u-sm-size1of2 u-lg-size1of4 u-ie-size1of4';
        break;

        case 'signup_cta_form_social_links':
          $grid_cell_classes = 'u-lg-size1of2 u-ie-size1of2';
        break;
      }

      if (!empty($grid_cell_classes)) {
        $variables['box_prefix'] = '<div class="Grid-cell ' . $grid_cell_classes . '">';
        $variables['box_suffix'] = '</div>';
      }
    break;
  }

  // Add specific container classes.
  if ($pane->type == 'ipa_products_list') {
    $variables['container_attributes_array']['class'][] = 'Container--productList';
    $variables['attributes_array']['class'][] = 'show-' . count($variables['content']);
  }
  elseif ($pane->type == 'ipa_core_map') {
    $variables['container_attributes_array']['class'][] = 'Container--map';
    $variables['container_attributes_array']['class'][] = 'Container--noPadding';
    $variables['container_attributes_array']['class'][] = 'Container--fullWidth';
  }
}

/**
 * Process variables for theme panels_pane.
 */
function ipa_process_panels_pane(&$variables) {
  // Add container classes to container attributes
  if (!empty($variables['container_attributes_array'])) {
    $variables['container_attributes'] = drupal_attributes($variables['container_attributes_array']);
  }
}

/**
 * Preprocess variables for theme menu_link.
 */
function ipa_preprocess_menu_link(&$variables) {
  // Add a class to menu items so that we can style the 'Home' link differently.
  $title = strtolower($variables['element']['#title']);
  $variables['element']['#attributes']['class'][] = 'Nav-item--' . minima_camelcase($title);
}

/**
 * Preprocess variables for theme minima_page_layout.
 */
function ipa_preprocess_minima_page_layout(&$variables) {
  if ($variables['display']->layout === '1_column') {
    $variables['minima_layout'] = array(
      'primary' => array(
        '#markup' => $variables['content']['primary'],
      ),
    );
  }
}

/**
 * Preprocess variables for theme htmlmail.
 */
function ipa_preprocess_htmlmail(&$variables) {
  $variables['less_variables']['style_guide_path'] = '~"' . libraries_get_path('style_guide_ipa') . '"';
}
