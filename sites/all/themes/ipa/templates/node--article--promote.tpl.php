<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <div<?php print $content_attributes; ?>>
      <div class="Grid-cell u-lg-size1of2 article-image">
        <?php if (!empty($image)): ?>
          <?php print $image; ?>
        <?php endif; ?>
      </div>
      <div class="Grid-cell u-lg-size1of2">
        <div class="article-details">
          <?php if ($display_title): ?>
            <h2 class="Node-title">
              <?php print $title; ?>
            </h2>
          <?php endif; ?>
          <div class="date">
            <?php print $date; ?>
          </div>
          <?php print $body; ?>
          <?php if (!empty($links)): ?>
            <?php print $links; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
