<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <div<?php print $content_attributes; ?>>
      <h2 class="Node-title">
        <?php print $title; ?>
        <small><?php print $product_abv_value; ?>%</small>
      </h2>
      <div class="Product-image">
        <a <?php print $link_attributes ?>>
          <?php print render($field_product_teaser_image); ?>
        </a>
      </div>
      <div class="Product-description">
        <?php print render($body) ?>
      </div>
      <?php if (!empty($links)): ?>
        <?php print $links; ?>
      <?php endif; ?>
    </div>
  </div>
<?php endif; ?>
