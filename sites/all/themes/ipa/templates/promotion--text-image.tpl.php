<div<?php print $attributes; ?>>
  <?php if ($is_link): ?><a<?php print $link_attributes; ?>><?php endif; ?>
    <div class="Promotion-body">
    	<h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
    	<div><?php print $body; ?></div>
	    <?php if ($cta): ?>
	      <div class="Promotion-cta Button"><?php print $cta; ?></div>
	    <?php endif; ?>
    </div>
  <?php if ($is_link): ?></a><?php endif; ?>
</div>
