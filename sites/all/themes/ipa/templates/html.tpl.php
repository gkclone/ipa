<?php

/**
 * @file
 * Minima theme implementation to display the basic html structure of a single
 * Drupal page.
 */
?><!DOCTYPE html>
<html<?php print $html_attributes; ?>>
<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <meta name="viewport" content="width=device-width">
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body<?php print $attributes; ?>>
  <div class="OffCanvas">
    <div class="OffCanvas-page">
      <?php print $page_top; ?>
      <?php print $page; ?>
      <?php print $page_bottom; ?>
    </div>
  </div>
</body>
</html>
