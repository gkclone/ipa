<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>
    <style type="text/css">
      <?php print $ink_css; ?>
      <?php print $custom_css; ?>
    </style>
  </head>

  <body>
    <table<?php print $attributes; ?>>
      <tr>
        <td class="center" align="center" valign="top">
          <center>

            <!-- Header start -->
            <table class="container header">
              <tr>
                <td>
                  <table class="row">
                    <tr>
                      <td class="wrapper last">
                        <table class="twelve columns">
                          <tr>
                            <td class="center">
                              <center>
                                <?php print $logo; ?>
                              </center>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <!-- Header end -->

            <!-- Content start -->
            <table class="container content">
              <tr>
                <td>
                  <!-- Content banner start -->
                  <?php if (!empty($banner)): ?>
                    <table class="row content__banner">
                      <tr>
                        <td class="wrapper last">
                          <table class="twelve columns">
                            <tr>
                              <td>
                                <?php print $banner; ?>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  <?php endif; ?>
                  <!-- Content banner end -->

                  <!-- Content body start -->
                  <table class="row content__body">
                    <tr>
                      <td class="wrapper last">
                        <table class="twelve columns">
                          <tr>
                            <td class="text-pad">
                              <?php print $body; ?>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                  <!-- Content body end -->
                </td>
              </tr>
            </table>
            <!-- Content end -->

            <!-- Footer start -->
            <table class="container footer">
              <tr>
                <td>
                  <table class="row">
                    <tr>
                      <td class="wrapper last">
                        <table class="twelve columns">
                          <tr>
                            <td class="center">
                              <center>
                                <?php print $footer; ?>
                              </center>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <!-- Footer end -->


          </center>
        </td>
      </tr>
    </table>
  </body>
</html>
