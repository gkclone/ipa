<div class="Container Container--cookies" data-offcanvas-fixed="bottom">
  <div class="Container-inner">
    <div class="CookiesMessage">
      <div class="Grid Grid--spaceHorizontal">
        <div class="Grid-cell u-md-size3of4 u-ie-size3of4">
          <?php if (!empty($title)): ?>
            <h2 class="CookiesMessage-title"><?php print $title; ?></h2>
          <?php endif; ?>

          <div class="CookiesMessage-message">
            <?php print $message; ?>
          </div>
        </div>

        <div class="Grid-cell u-md-size1of4 u-ie-size1of4">
          <ul class="CookieOptions">
            <?php if (!empty($more_link)): ?>
              <li class="cookies__more"><?php print $more_link; ?></li>
            <?php endif; ?>
            <li class="CookieOptions-continue">
              <a class="Button Button--secondary" href="/">Continue</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
