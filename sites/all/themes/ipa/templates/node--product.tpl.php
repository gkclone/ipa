<div class="Grid Grid--spaceHorizontal Product">
	<div class="Grid-cell u-lg-size2of5 u-ie-size2of5 Product-image">
		<?php print render($field_product_full_image) ?>
	</div>
	<div class="Grid-cell  u-lg-size3of5 u-ie-size3of5">
		<h1><?php print $title ?></h1>

		<div class="Product-abv">
			<span>ABV <?php print $product_abv_value ?>%</span>
		</div>

		<?php print render($body) ?>

		<table class="Product-table">
			<tbody>
				<tr>
					<th>Bitter</th>
					<td><div class="star-rating" data-rating="<?php print $product_bitter_value ?>"><?php print $product_bitter_value ?></div></td>
				</tr>

				<tr>
					<th>Sweet</th>
					<td><div class="star-rating" data-rating="<?php print $product_sweet_value ?>"><?php print $product_sweet_value ?></div></td>
				</tr>

				<?php if ($field_product_appearance) : ?>
					<tr>
						<th>Appearance</th>
						<td><?php print render($field_product_appearance) ?></td>
					</tr>
				<?php endif ?>

				<?php if ($field_product_smell) : ?>
					<tr>
						<th>Smell</th>
						<td><?php print render($field_product_smell) ?></td>
					</tr>
				<?php endif ?>

				<?php if ($field_product_taste) : ?>
					<tr>
						<th>Taste</th>
						<td><?php print render($field_product_taste) ?></td>
					</tr>
				<?php endif ?>
			</tbody>
		</table>

		<?php if ($field_product_ingredients) : ?>
			<h3>What's in it?</h3>
			<?php print render($field_product_ingredients) ?>
		<?php endif ?>

		<?php if ($field_product_food_pairing) : ?>
			<h3>Food pairing</h3>
			<?php print render($field_product_food_pairing) ?>
		<?php endif ?>

		<?php if ($field_product_available_in) : ?>
			<h3>Available in</h3>
			<?php print render($field_product_available_in) ?>
		<?php endif ?>

		<?php print $shop_link; ?>
	</div>
</div>
