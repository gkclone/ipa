<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <div<?php print $content_attributes; ?>>
      <?php if (!empty($sharethis)): ?>
        <?php print $sharethis; ?>
      <?php endif; ?>
      <div class="date">
        <?php print $date; ?>
      </div>
      <?php print $body; ?>
      <?php print $field_article_categories; ?>
    </div>
  </div>
<?php endif; ?>
