<?php
/**
 * @file
 * ipa_core.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function ipa_core_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template_panel_context';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -30;
  $handler->conf = array(
    'title' => 'IPA: Default',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'ipa_core_site_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'header' => NULL,
      'navigation' => NULL,
      'content' => NULL,
      'footer' => NULL,
      'footer_bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '696b38ee-96d9-439e-b3f3-78dcb036967e';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-d845e5cf-b307-4e5e-a359-79682f46856a';
    $pane->panel = 'content';
    $pane->type = 'page_content';
    $pane->subtype = 'page_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_page_content_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd845e5cf-b307-4e5e-a359-79682f46856a';
    $display->content['new-d845e5cf-b307-4e5e-a359-79682f46856a'] = $pane;
    $display->panels['content'][0] = 'new-d845e5cf-b307-4e5e-a359-79682f46856a';
    $pane = new stdClass();
    $pane->pid = 'new-633fd387-2412-4dba-969c-ef4bb6fa883c';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'menu-footer-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Site menu',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '633fd387-2412-4dba-969c-ef4bb6fa883c';
    $display->content['new-633fd387-2412-4dba-969c-ef4bb6fa883c'] = $pane;
    $display->panels['footer'][0] = 'new-633fd387-2412-4dba-969c-ef4bb6fa883c';
    $pane = new stdClass();
    $pane->pid = 'new-aeb7c9a1-e423-470c-84a8-a6f4ce7e5cf2';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-ipa-core-footer-secondary-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Helpful links',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'aeb7c9a1-e423-470c-84a8-a6f4ce7e5cf2';
    $display->content['new-aeb7c9a1-e423-470c-84a8-a6f4ce7e5cf2'] = $pane;
    $display->panels['footer'][1] = 'new-aeb7c9a1-e423-470c-84a8-a6f4ce7e5cf2';
    $pane = new stdClass();
    $pane->pid = 'new-05e325b3-2b9a-4263-9252-bd8e03a511e5';
    $pane->panel = 'footer';
    $pane->type = 'signup_cta_form_social_links';
    $pane->subtype = 'signup_cta_form_social_links';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Sign up',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '05e325b3-2b9a-4263-9252-bd8e03a511e5';
    $display->content['new-05e325b3-2b9a-4263-9252-bd8e03a511e5'] = $pane;
    $display->panels['footer'][2] = 'new-05e325b3-2b9a-4263-9252-bd8e03a511e5';
    $pane = new stdClass();
    $pane->pid = 'new-8cb86c67-536e-49d6-88e4-39f94b60662d';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'gk_core-copyright';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '8cb86c67-536e-49d6-88e4-39f94b60662d';
    $display->content['new-8cb86c67-536e-49d6-88e4-39f94b60662d'] = $pane;
    $display->panels['footer_bottom'][0] = 'new-8cb86c67-536e-49d6-88e4-39f94b60662d';
    $pane = new stdClass();
    $pane->pid = 'new-fa2d9ef0-b269-4bb7-aa71-a6940862a625';
    $pane->panel = 'header';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'fa2d9ef0-b269-4bb7-aa71-a6940862a625';
    $display->content['new-fa2d9ef0-b269-4bb7-aa71-a6940862a625'] = $pane;
    $display->panels['header'][0] = 'new-fa2d9ef0-b269-4bb7-aa71-a6940862a625';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-fa2d9ef0-b269-4bb7-aa71-a6940862a625';
  $handler->conf['display'] = $display;
  $export['site_template_panel_context'] = $handler;

  return $export;
}
