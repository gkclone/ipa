<?php

// Plugin definition
$plugin = array(
  'title' => t('IPA: Site default'),
  'category' => t('Site'),
  'icon' => 'ipa_core_site_default.png',
  'theme' => 'minima_site_layout',
  'regions' => array(
    'header' => t('Header'),
    'content' => t('Content'),
    'footer' => t('Footer'),
    'footer_bottom' => t('Footer bottom'),
  ),
);

/**
 * Implements minima_layout_LAYOUT_NAME().
 */
function minima_layout_ipa_core_site_default($variables) {
  $layout = array(
    'page_container' => array(
      '#theme_wrappers' => array('container'),
      '#attributes' => array(
        'class' => array('SiteContainer'),
      ),
      'header_container' => array(
        '#theme_wrappers' => array('minima_container'),
        '#container_element' => 'header',
        '#container_attributes' => array(
          'class' => array('Container--header'),
        ),
        'header' => array(
          '#markup' => $variables['content']['header'],
        ),
      ),
      'main' => array(
        '#theme_wrappers' => array('container'),
        '#attributes' => array(
          'class' => array('PageContainer'),
        ),
        '#markup' => $variables['content']['content'],
      ),
      'footer_container' => array(
        '#theme_wrappers' => array('minima_grid', 'minima_container'),
        '#container_element' => 'footer',
        '#container_attributes' => array(
          'class' => array('Container--footer'),
        ),
        '#grid_attributes' => array(
          'class' => array('Grid--space'),
        ),
        'footer' => array(
          '#markup' => $variables['content']['footer'],
        ),
      ),
      'footer_bottom_container' => array(
        '#theme_wrappers' => array('minima_grid', 'minima_container'),
        '#container_attributes' => array(
          'class' => array('Container--footerBottom'),
        ),
        'footer_bottom' => array(
          '#theme_wrappers' => array('minima_grid_cell'),
          '#markup' => $variables['content']['footer_bottom'],
        ),
      ),
    ),
  );

  // Check the current node for a header background image.
  if ($node = menu_get_object()) {
    if ($field = field_get_items('node', $node, 'field_header_background_image')) {
      $url = file_create_url($field[0]['uri']);
      $layout['page_container']['#attributes']['style'] = 'background-image: url(' . $url . ');';
    }
  }

  // If current page isn't handled by page manager wrap content in a container.
  $current_page = page_manager_get_current_page();

  if (empty($current_page)) {
    $layout['page_container']['main'] = array(
      'main' => array(
        '#theme_wrappers' => array('container'),
        '#attributes' => array(
          'class' => array('PageContainer'),
        ),
        'content_header' => array(
          '#theme' => 'pane_content_header'
        ),
        'content' => array(
          '#markup' => $variables['content']['content'],
          '#theme_wrappers' => array('minima_container'),
          '#container_element' => 'main',
          '#container_attributes' => array(
            'class' => array('Container--main'),
          ),
        ),
      ),
    );
  }

  return $layout;
}
