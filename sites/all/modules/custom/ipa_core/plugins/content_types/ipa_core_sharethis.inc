<?php

$plugin = array(
  'title' => 'Core: ShareThis',
  'category' => 'IPA Core',
  'single' => TRUE,
);

function ipa_core_ipa_core_sharethis_content_type_render($subtype, $conf, $args, $context) {
  $content = '';

  if ($node = menu_get_object()) {
    $options = variable_get('sharethis_node_types');

    if (!empty($options[$node->type])) {
      $content = theme('sharethis', array(
        'data_options' => sharethis_get_options_array(),
        'm_path' => url('node/' . arg(1), array('absolute' => TRUE)),
        'm_title' => decode_entities(drupal_get_title()),
      ));
    }
  }

  return (object) array(
    'title' => '',
    'content' => array(
      '#markup' => $content,
    ),
  );
}

function ipa_core_ipa_core_sharethis_content_type_edit_form($form, &$form_state) {
  return $form;
}

function ipa_core_ipa_core_sharethis_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
}
