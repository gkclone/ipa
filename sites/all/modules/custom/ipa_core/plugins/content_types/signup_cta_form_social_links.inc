<?php

$plugin = array(
  'title' => 'Core: Sign-up CTA form/Social links',
  'category' => 'IPA Core',
  'single' => TRUE,
);

function ipa_core_signup_cta_form_social_links_content_type_render($subtype, $conf, $args, $context) {
  $content = array(
    'signup_cta_form' => drupal_get_form('gk_members_signup_cta_form'),
  );

  // Embed the gk_social_links plugin from the gk_social module.
  ctools_plugin_load_function('ctools', 'content_types', 'gk_social_links', 'render callback');

  if ($social_links = gk_social_gk_social_links_content_type_render($subtype, $conf, $args, $context)) {
    if (!empty($social_links->content)) {
      $content['social_links'] = array(
        'box' => array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('Box', 'Box--socialLinks'),
          ),
          'box_title' => array(
            '#theme' => 'html_tag',
            '#tag' => 'h2',
            '#value' => 'Follow us',
            '#attributes' => array(
              'class' => array('Box-title'),
            ),
          ),
          'box_content' => array(
            '#theme_wrappers' => array('container'),
            '#attributes' => array(
              'class' => 'Box-content',
            ),
            'content' => $social_links->content,
          ),
        )
      );
    }
  }

  return (object) array(
    'title' => '',
    'content' => $content,
  );
}

function ipa_core_signup_cta_form_social_links_content_type_edit_form($form, &$form_state) {
  return $form;
}

function ipa_core_signup_cta_form_social_links_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
}
