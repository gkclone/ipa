<?php

$plugin = array(
  'title' => 'Core: Map',
  'category' => 'IPA Core',
  'single' => TRUE,
);

function ipa_core_ipa_core_map_content_type_render($subtype, $conf, $args, $context) {
  return (object) array(
    'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
    'content' => ipa_core_build_map(array(
      'center' => array(
        'geo' => array(
          'latitude' => $conf['latitude'],
          'longitude' => $conf['longitude'],
        ),
        'info_window' => $conf['info_window'],
      ),
      'zoom' => intval($conf['zoom']),
      'width' => intval($conf['width']),
      'height' => intval($conf['height']),
      'scrollwheel' => intval($conf['scrollwheel']),
    )),
  );
}

function ipa_core_ipa_core_map_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['latitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Latitude'),
    '#default_value' => isset($conf['latitude']) ? $conf['latitude'] : '',
  );

  $form['longitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Longitude'),
    '#default_value' => isset($conf['longitude']) ? $conf['longitude'] : '',
  );

  $form['info_window'] = array(
    '#type' => 'textfield',
    '#title' => t('Info window'),
    '#default_value' => isset($conf['info_window']) ? $conf['info_window'] : '',
  );

  $form['zoom'] = array(
    '#title' => t('Zoom'),
    '#type' => 'select',
    '#options' => drupal_map_assoc(range(1, 20)),
    '#default_value' => isset($conf['zoom']) ? $conf['zoom'] : 12,
    '#required' => TRUE,
  );

  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => isset($conf['width']) ? $conf['width'] : NULL,
  );

  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => isset($conf['height']) ? $conf['height'] : 350,
  );

  $form['scrollwheel'] = array(
    '#title' => t('Allow scrollwheel zoom?'),
    '#type' => 'checkbox',
    '#default_value' => isset($conf['scrollwheel']) ? $conf['scrollwheel'] : 0,
  );

  return $form;
}

function ipa_core_ipa_core_map_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
}
