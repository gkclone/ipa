<?php

$plugin = array(
  'title' => 'Core: Text with background image',
  'category' => 'IPA Core',
  'single' => TRUE,
);

function ipa_core_text_background_image_content_type_render($subtype, $conf, $args, $context) {
  $content = array(
    'text' => array(
      '#markup' => check_markup($conf['text']['value'], $conf['text']['format']),
    ),
  );

  if ($image = file_load($conf['image'])) {
    $content['background_image'] = $image->uri;
  }

  if (isset($conf['container_class'])) {
    $content['container_class'] = $conf['container_class'];
  }

  if (isset($conf['container_id'])) {
    $content['container_id'] = $conf['container_id'];
  }

  return (object) array(
    'title' => '',
    'content' => $content,
  );
}

function ipa_core_text_background_image_content_type_edit_form($form, &$form_state) {
  // This is required because Drupal won't load this file the next time it needs
  // to build this form (i.e. during AJAX requests invoked by the file field).
  form_load_include($form_state, 'inc', 'ipa_core', 'plugins/content_types/text_background_image');

  $conf = $form_state['conf'];
  $text = isset($conf['text']) ? $conf['text'] : NULL;

  $form['text'] = array(
    '#type' => 'text_format',
    '#title' => 'Text',
    '#default_value' => $text ? $text['value'] : '',
    '#format' => $text ? $text['format'] : NULL,
  );

  $form['container_class'] = array(
    '#type' => 'textfield',
    '#title' => 'Container class',
    '#default_value' => isset($conf['container_class']) ? $conf['container_class'] : '',
  );

  $form['container_id'] = array(
    '#type' => 'textfield',
    '#title' => 'Container ID',
    '#default_value' => isset($conf['container_id']) ? $conf['container_id'] : '',
  );

  $form['image'] = array(
    '#title' => t('Image'),
    '#type' => 'managed_file',
    '#upload_location' => 'public://panes/',
    '#default_value' => isset($conf['image']) ? $conf['image'] : NULL,
    '#weight' => -10,
  );

  return $form;
}

function ipa_core_text_background_image_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];

  // Make the uploaded file permanent.
  if ($file = file_load($form_state['values']['image'])) {
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    file_usage_add($file, 'ipa_core', 'pane', $file->fid);
  }
}
