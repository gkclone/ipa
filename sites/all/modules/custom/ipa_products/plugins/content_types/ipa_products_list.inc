<?php

$plugin = array(
  'title' => 'Products: List',
  'category' => 'IPA Products',
  'single' => TRUE,
);

function ipa_products_ipa_products_list_content_type_render($subtype, $conf, $args, $context) {
  // Build an array of options to pass to ipa_products_get_items().
  $options = array(
    'exclude_current_node' => $conf['exclude_current_node'],
  );

  if ($products = ipa_products_get_items($options)) {
    $content = array();

    foreach ($products as $product) {
      $content[$product->nid] = node_view($product, 'teaser');
    }

    return (object) array(
      'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
      'content' => $content,
    );
  }
}

function ipa_products_ipa_products_list_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['exclude_current_node'] = array(
    '#type' => 'checkbox',
    '#title' => 'Exclude current node',
    '#default_value' => isset($conf['exclude_current_node']) ? $conf['exclude_current_node'] : 1,
  );

  return $form;
}

function ipa_products_ipa_products_list_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
}
