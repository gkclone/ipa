<?php
/**
 * @file
 * ipa_products.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function ipa_products_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:product:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'product';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '1_column';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '8984ca86-0846-4aaf-b3dc-6d157eb543a5';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-73bf244a-aa60-4bf4-abcf-3d931114aa92';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '73bf244a-aa60-4bf4-abcf-3d931114aa92';
    $display->content['new-73bf244a-aa60-4bf4-abcf-3d931114aa92'] = $pane;
    $display->panels['primary'][0] = 'new-73bf244a-aa60-4bf4-abcf-3d931114aa92';
    $pane = new stdClass();
    $pane->pid = 'new-202c52de-1b93-440f-8165-536fd5315bea';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '202c52de-1b93-440f-8165-536fd5315bea';
    $display->content['new-202c52de-1b93-440f-8165-536fd5315bea'] = $pane;
    $display->panels['primary'][1] = 'new-202c52de-1b93-440f-8165-536fd5315bea';
    $pane = new stdClass();
    $pane->pid = 'new-3873016e-dcb1-4965-a2f3-5dcd887bf7d9';
    $pane->panel = 'primary';
    $pane->type = 'ipa_core_sharethis';
    $pane->subtype = 'ipa_core_sharethis';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '3873016e-dcb1-4965-a2f3-5dcd887bf7d9';
    $display->content['new-3873016e-dcb1-4965-a2f3-5dcd887bf7d9'] = $pane;
    $display->panels['primary'][2] = 'new-3873016e-dcb1-4965-a2f3-5dcd887bf7d9';
    $pane = new stdClass();
    $pane->pid = 'new-f0b4d923-706a-4be4-840f-0a74465f9174';
    $pane->panel = 'primary';
    $pane->type = 'ipa_products_list';
    $pane->subtype = 'ipa_products_list';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'See our other beers',
      'exclude_current_node' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'f0b4d923-706a-4be4-840f-0a74465f9174';
    $display->content['new-f0b4d923-706a-4be4-840f-0a74465f9174'] = $pane;
    $display->panels['primary'][3] = 'new-f0b4d923-706a-4be4-840f-0a74465f9174';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:product:default'] = $panelizer;

  return $export;
}
