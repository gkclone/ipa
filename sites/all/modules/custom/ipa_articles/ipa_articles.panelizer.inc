<?php
/**
 * @file
 * ipa_articles.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function ipa_articles_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:article:ipa_default';
  $panelizer->title = 'IPA: Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'article';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '2_columns_stacked_75_25';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
      'secondary' => NULL,
      'top' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '19166602-490b-4cd3-bbd9-6bd32f9c39b6';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b339bda8-6bf9-4e1e-bb46-772180f3c73c';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b339bda8-6bf9-4e1e-bb46-772180f3c73c';
    $display->content['new-b339bda8-6bf9-4e1e-bb46-772180f3c73c'] = $pane;
    $display->panels['primary'][0] = 'new-b339bda8-6bf9-4e1e-bb46-772180f3c73c';
    $pane = new stdClass();
    $pane->pid = 'new-00fb43e4-befe-4c28-830b-6d18b6ed63fd';
    $pane->panel = 'secondary';
    $pane->type = 'gk_articles_categories';
    $pane->subtype = 'gk_articles_categories';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Other Categories',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '00fb43e4-befe-4c28-830b-6d18b6ed63fd';
    $display->content['new-00fb43e4-befe-4c28-830b-6d18b6ed63fd'] = $pane;
    $display->panels['secondary'][0] = 'new-00fb43e4-befe-4c28-830b-6d18b6ed63fd';
    $pane = new stdClass();
    $pane->pid = 'new-73ee5ede-4277-45e3-a57c-8717df958036';
    $pane->panel = 'top';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '73ee5ede-4277-45e3-a57c-8717df958036';
    $display->content['new-73ee5ede-4277-45e3-a57c-8717df958036'] = $pane;
    $display->panels['top'][0] = 'new-73ee5ede-4277-45e3-a57c-8717df958036';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:article:ipa_default'] = $panelizer;

  return $export;
}
